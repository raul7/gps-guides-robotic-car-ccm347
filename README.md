## GPS GUIDES ROBOTIC CAR

Author: Raul Alvarez-Torrico

This is the source code for my Circuit Cellar article "GPS Guides Robotic Car - Arduino UNO in Action" (#347, June 2019).

13/1/2022:
- Gérard Pays reported back that the compilation works using: 'jrowberg/i2cdevlib' for the HMC5883L compass.
10/8/2022:
- I added "TinyGPS", "I2Cdev" and "HMC5883L" libraries locally to the source code to facilitate compilation. At the time of this update, "I2Cdev" can't be easily installed from the Arduino IDE (see https://github.com/jrowberg/i2cdevlib). This new version compiles fine, but I didn't tested it with the robotic car.